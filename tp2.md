# TP2 : Routage, DHCP et DNS

# 0. Setup

- **SSH fonctionnel :** 
  - Dans un  powershell ➜ $ssh hostname@adresse_du_serveur.
  
  - Dans ce tp, nous allons définir une ip statique (adresse du serveur de la vm) pour chaques interfaces qui ne sont pas branchées sur gns3, pour chaques vms, dans le même réseau donc que l'ip de la carte réseau créée dans virtualbox.
  
- **Donner un nom d'hôte à la VM :**
  - Dans une vm ➜  
```bash
# Changer de nom d’hôte
$ sudo hostnamectl set-hostname nouveau-nom-d-hote

# Vérifier le nouveau nom d’hôte 
$ hostname

# Relancer la machine pour voir le nom d’hôte apparaître
$ reboot 
```

# I. Routage

➜ **Tableau d'adressage**

| Nom                | IP              |
| ------------------ | --------------- |
| `router.tp2.efrei` | `10.2.1.254/24` |
| `node1.tp2.efrei`  | `10.2.1.1/24`   |

➜ **Reproduire la topologie dans GNS3** :

- **Ajouter une carte réseau à `router.tp2.efrei` :**

  - Dans GNS3 ➜ (Edit, Preferences, VirtualBox VMs, router.tp2.efrei, Edit, Network, Adapters: 3)

- **Mise en place du nuage NAT dans la topologie**

☀️ **Configuration de `router.tp2.efrei`**

- **Setup l'interface branchée au NAT pour qu'elle récup une IP en DHCP**:
  
```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On regarde les fichiers existants
$ ls 

# On modifie le fichier
$ nano ifcfg-enp0s9 
```
- **Config de l'interface :**

```bash
NAME=enp0s9
DEVICE=enp0s9

BOOTPROTO=dhcp
ONBOOT=yes
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s9
```

- **Prouver l'accès internet en une commande `ping` depuis le routeur**
  
  - $ping google.com

- **Setup statiquement l'autre interface de `router.tp2.efrei` :** 

  - $nano ifcfg-enp0s3
  
    - **Config de l'interface :**
```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.254
NETMASK=255.255.255.0
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s3
```

- **Résultat de la config du routeur :**

    - $ip a

        - **Affichage :**
  
```bash
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:5b:d9:fe brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe5b:d9fe/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3e:c7:04 brd ff:ff:ff:ff:ff:ff
    inet 192.168.194.2/24 brd 192.168.194.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe3e:c704/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:79:3f:69 brd ff:ff:ff:ff:ff:ff
    inet 192.168.150.130/24 brd 192.168.150.255 scope global dynamic noprefixroute enp0s9
       valid_lft 1190sec preferred_lft 1190sec
    inet6 fe80::a00:27ff:fe79:3f69/64 scope link
       valid_lft forever preferred_lft forever
```

Aussi, on va demander à cette machine Rocky de ne pas jeter les paquets IPs qui ne lui sont pas destinés, **afin qu'elle puisse agir comme un routeur**.

Pour ça, deux commandes à exécuter sur `router.tp2.efrei` :

```bash
$ cd
# On active le forwarding IPv4
$ sudo sysctl -w net.ipv4.ip_forward=1 
net.ipv4.ip_forward = 1

# Petite modif du firewall qui nous bloquerait sinon
$ sudo firewall-cmd --add-masquerade
success

# Et on tape aussi la même commande une deuxième fois, en ajoutant --permanent pour que ce soit persistent après un éventuel reboot
$ sudo firewall-cmd --add-masquerade --permanent
success
```

☀️ **Configuration de `node1.tp2.efrei`**

- **configurer de façon statique son IP :**

```bash
$ cd /etc/sysconfig/network-scripts (on rentre dans network-scripts pour pouvoir accéder à l’interface enp0s3

$ ls (on regarde si le fichier ifcfg-enp0s3 est déjà créé, pour ma part, il ne l’est pas)

$ sudo su (on se met en root pour avoir les permissions de modification du fichier

$ nano ifcfg-enp0s3 (on créé le fichier afin de le modifier)
```

- **Config de l'interface :**
  
```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0
```
- **Prouver avec une commande `ping` que `node1.tp2.efrei` peut joindre `router.tp2.efrei` :**

    - $ping 10.2.1.253
  
- **Affichage :**

```bash
PING 10.2.1.253 (10.2.1.253) 56(84) bytes of data.
64 bytes from 10.2.1.253: icmp_seq=1 ttl=64 time=0.124 ms
64 bytes from 10.2.1.253: icmp_seq=2 ttl=64 time=0.481 ms
--- 10.2.1.253 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1057ms
rtt min/avg/max/mdev = 0.124/0.302/0.481/0.178 ms
```

- **Ajouter une route par défaut qui passe par `router.tp2.efrei` :**

    - Nous allons ajouter à la config de l'interface enp0s3 de node1, l'adresse ip, définie pour l'interface du routeur, branchée au switch.

```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On regarde les fichiers existants
$ ls 

# On modifie le fichier
$ nano ifcfg-enp0s3 
```

- **Config de l'interface :**

```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0

GATEWAY=192.168.1.254
DNS1=1.1.1.1
```
   
- **Prouver que vous avez un accès internet depuis `node1.tp2.efrei` désormais, avec une commande `ping` :**

    - $ping google.com

- **Affichage :**

```bash
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=127 time=19.6 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=127 time=37.2 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=3 ttl=127 time=25.7 ms
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 19.570/27.511/37.239/7.323 ms
```

- **Utiliser une commande `traceroute` pour prouver que vos paquets passent bien par `router.tp2.efrei` avant de sortir vers internet :**

  - $ip r s

- **Affichage :**

```bash
default via 10.2.1.254 dev enp0s3 proto dhcp src 10.2.1.20 metric 100
10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.20 metric 100
192.168.194.0/24 dev enp0s8 proto kernel scope link src 192.168.194.4 metric 101
```

# II. Serveur DHCP


➜ **Tableau d'adressage**

| Nom                | IP              |
| ------------------ | --------------- |
| `router.tp2.efrei` | `10.2.1.254/24` |
| `node1.tp2.efrei`  | `N/A`           |
| `dhcp.tp2.efrei`   | `10.2.1.253/24` |

☀️ **Install et conf du serveur DHCP** sur `dhcp.tp2.efrei`

- **Ajouter une route par défaut, qui passe par `router.tp2.efrei` :**

  - $cd /etc/sysconfig/network-scripts/

  - $nano ifcfg-enp0s3

- **Config de l'interface :**

```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.253
NETMASK=255.255.255.0

GATEWAY=10.2.1.254
DNS1=1.1.1.1
```

  - $nmcli con reload

  - $nmcli con up enp0s3

- **Résultat de la config :**

    - $ping google.com

- **Affichage :**

```bash
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=127 time=23.9 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=127 time=22.9 ms
```
- **Install du serveur :** 

    - $dnf -y install dhcp-server

- **Ajouter, dans la conf, une option DHCP pour donner au client l'adresse de la passerelle du réseau (c'est à dire l'adresse de `router.tp2.efrei`) en plus de leur proposer une IP libre :**

    - $vi /etc/dhcp/dhcpd.conf

```bash
# create new
# specify domain name
option domain-name     "tp2.efrei";

# specify DNS server's hostname or IP address
option domain-name-servers     1.1.1.1;

# default lease time
default-lease-time 600;

# max lease time
max-lease-time 7200;

# this DHCP server to be declared valid
authoritative;

# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.20 10.2.1.252;
    # specify broadcast address
    option broadcast-address 10.2.1.255;
    # specify gateway
    option routers 10.2.1.254;
}
```

$systemctl enable --now dhcpd


- **Autoriser le service DHCP :**

  - $firewall-cmd --add-service=dhcp

  ```bash
  success
  ```

  - $firewall-cmd --runtime-to-permanent

  ```bash
  success
  ```


☀️ **Test du DHCP** sur `node1.tp2.efrei`

- Modifier l'interface pour qu'elle récupère une IP dynamique, c'est à dire avec DHCP

  - $cd /etc/sysconfig/network-scripts/

  - $nano ifcfg-enp0s3

- **Config de l'interface :**

```bash 
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes
```

$nmcli con reload

$nmcli con up enp0s3

- vérifiez que :

  - l'IP obtenue est correcte
  ```bash
  $ip a

  2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c0:d3:72 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.20/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 540sec preferred_lft 540sec
    inet6 fe80::a00:27ff:fec0:d372/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
  ```

  - votre table de routage a bien été mise à jour automatiquement avec l'adresse de la passerelle en route par défaut (votre option DHCP a bien été reçue !)

  ```bash
  $ip r s

  default via 10.2.1.254 dev enp0s3 proto dhcp src 10.2.1.20 metric 100
  10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.20 metric 100
  192.168.194.0/24 dev enp0s8 proto kernel scope link src 192.168.194.4 metric 101
  ```

  - vous pouvez immédiatement joindre internet

  ```bash
  $ping google.com

  PING google.com (142.250.74.238) 56(84) bytes of data.
  64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=127 time=23.9 ms
  64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=127 time=22.9 ms
  ```


🌟 **BONUS**

- **Ajouter une autre ligne dans la conf du serveur DHCP pour qu'il donne aussi l'adresse d'un serveur DNS (utilisez `1.1.1.1` comme serveur DNS) :**

  - Voir dans la conf DHCP un peu plus haut.

# III. ARP

## 1. Les tables ARP

☀️ **Affichez la table ARP de `router.tp2.efrei`**

- **Vérifier la présence des IP et MAC de `node1.tp2.efrei` et `dhcp.tp2.efrei`

  - $ip neigh show (dans `node1.tp2.efrei`)

- **Affichage :**

```bash
10.2.1.253 dev enp0s3 lladdr 08:00:27:c6:2b:01 STALE
```
Nous pouvons voir ici l'IP et la MAC de `dhcp.tp2.efrei`

  - $ip neigh show (dans `dhcp.tp2.efrei`)

**Affichage :**

```bash
10.2.1.20 dev enp0s3 lladdr 08:00:27:c0:d3:72 STALE
```

Nous pouvons voir, à l'inverse ici, l'IP et la MAC de `node1.tp2.efrei`

- **Echange ARP :**

    - $ping 10.2.1.253 (par exemple)

## 2. ARP poisoning

☀️ **Exécuter un simple ARP poisoning**

- **Ecrivez dans la table ARP de `node1` que l'adresse `10.2.1.254` correspond à l'adresse MAC de votre choix :**

  - $ip neigh show (avant la modif)

- **Affichage :**

```bash
10.2.1.254 dev enp0s3 lladdr 08:00:27:5b:d9:fe STALE
```

$ip neigh show (après la modif)

**Affichage :**

```bash
10.2.1.254 dev enp0s3 lladdr 08:00:27:c0:d3:72 PERMANENT
```

- **Conclusion de cette partie :**

  - J'ai usurpé l'identité de 10.2.1.254, donc du routeur, auprès de node1.

  
