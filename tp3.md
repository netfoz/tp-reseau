# TP3 : Vers un réseau d'entreprise

# Mise en place de la topologie et routage

### Tableau d'adressage

| Machine           | Réseau 1        | Réseau 2        | Réseau 3        |
| ----------------- | --------------- | --------------- | --------------- |
| `node1.net1.tp3`  | `10.3.1.11/24`  | nop             | nop             |
| `node2.net1.tp3`  | `10.3.1.12/24`  | nop             | nop             |
| `router.net1.tp3` | `10.3.1.254/24` | nop             | `10.3.100.1/30` |
| `router.net2.tp3` | nop             | `10.3.2.254/24` | `10.3.100.2/30` |
| `node1.net2.tp3`  | nop             | `10.3.2.11/24`  | nop             |
| `node2.net2.tp3`  | nop             | `10.3.2.12/24`  | nop             |

## I. Setup GNS3

🌞 **Mettre en place la topologie dans GS3**

- reproduisez la topologie, configurez les IPs et les noms sur toutes les machines

- **Donner un nom d'hôte à la VM :**
  - Dans une vm ➜  
```bash
# Changer de nom d’hôte
$ sudo hostnamectl set-hostname nouveau-nom-d-hote

# Vérifier le nouveau nom d’hôte 
$ hostname

# Relancer la machine pour voir le nom d’hôte apparaître
$ reboot 
```

- **configuration de façon statique une IP :**

```bash
# On va accéder aux différentes interface
$ cd /etc/sysconfig/network-scripts 

# On regarde si le fichier que nous souhaitons modifier est déjà créé
$ ls 

# On se met en root pour avoir les permissions de modification du fichier
$ sudo su 

# On créé le fichier afin de le modifier ou on le modifie directement
$ nano le_fichier_que_nous_souhaitons_modifier 
```
- Pour ma part, j'ai fait :

```bash
interface 1 = enp0s3
interface 2 = enp0s8
interface 3 = enp0s9
```
```bash
- node1 et node2 du réseau 1 ➜ modification et configuration de enp0s3

- node1 et node2 du réseau 2 ➜ modification et configuration de enp0s8

- router du réseau 1 et 3 ➜ modification et configuration de enp0s3 et de enp0s9

- router du réseau 2 et 3 ➜ modification et configuration de enp0s8 et de enp0s9
```

- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload

$ sudo nmcli con up nom_interface_configurée
```

- une fois en place, assurez-vous donc que :

  - toutes les machines du réseau 1 peuvent se `ping` entre elles

    - par exemple : node1.net1.tp3 peut ping node2.net1.tp3

- **Affichage :**

```bash
[root@Gregersen netfoz]# ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=4.58 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=2.98 ms
^C
--- 10.3.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 2.979/3.781/4.583/0.802 ms
```


  - toutes les machines du réseau 2 peuvent se `ping` entre elles
  
    - par exemple : node1.net2.tp3 peut ping node2.net2.tp3

- **Affichage :**

```bash
[root@Nsimba netfoz]# ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=14.6 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=64 time=1.72 ms
^C
--- 10.3.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.721/8.163/14.605/6.442 ms
```


  - toutes les machines du réseau 3 peuvent se `ping` entre elles
    
    - par exemple : router.net1.tp3 peut ping router.net2.tp3

- **Affichage :**

```bash
[root@Diaz netfoz]# ping 10.3.100.2
PING 10.3.100.2 (10.3.100.2) 56(84) bytes of data.
64 bytes from 10.3.100.2: icmp_seq=1 ttl=64 time=4.95 ms
64 bytes from 10.3.100.2: icmp_seq=2 ttl=64 time=2.67 ms
64 bytes from 10.3.100.2: icmp_seq=3 ttl=64 time=4.22 ms

--- 10.3.100.2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2007ms
rtt min/avg/max/mdev = 2.673/3.948/4.952/0.950 ms
```


☀️ **Configuration du `router1.tp3`**

- le `router1.tp3` doit avoir un accès internet normal

- **Setup de l'interface branchée au NAT pour qu'elle récup une IP en DHCP**:
  
```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On regarde les fichiers existants
$ ls 

# On modifie le fichier
$ nano ifcfg-enp0s8 
```
- **Config de l'interface :**

```bash
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=dhcp
ONBOOT=yes
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s8
```

- **prouvez avec une commande `ping` qu'il peut joindre une IP publique connue**

  - $ping google.com

- **Affichage :**

```bash
PING google.com (172.217.20.174) 56(84) bytes of data.
64 bytes from waw02s07-in-f174.1e100.net (172.217.20.174): icmp_seq=1 ttl=128 time=25.6 ms
64 bytes from par10s49-in-f14.1e100.net (172.217.20.174): icmp_seq=2 ttl=128 time=25.6 ms
64 bytes from waw02s07-in-f14.1e100.net (172.217.20.174): icmp_seq=3 ttl=128 time=50.1 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 25.605/33.795/50.135/11.553 ms
```
- **prouvez avec une commande `ping` qu'il peut joindre des machines avec leur nom DNS public (genre `efrei.fr`)**

  - $ping efrei.fr 

- **Affichage :**

```bash
PING efrei.fr (51.255.68.208) 56(84) bytes of data.
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=1 ttl=128 time=28.4 ms
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=2 ttl=128 time=30.9 ms
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=3 ttl=128 time=31.9 ms
^C
--- efrei.fr ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 28.396/30.392/31.891/1.469 ms
```

## II. Routes routes routes

🌞 **Activer le routage sur les deux machines `router`**

```bash
# On active le forwarding IPv4
$sudo sysctl -w net.ipv4.ip_forward=1 
net.ipv4.ip_forward = 1

# Petite modif du firewall qui nous bloquerait sinon
$sudo firewall-cmd --add-masquerade
success

# Et on tape aussi la même commande une deuxième fois, en ajoutant --permanent pour que ce soit persistent après un éventuel reboot
$sudo firewall-cmd --add-masquerade --permanent
success
```

🌞 **Mettre en place les routes locales**

- Ajouter les routes nécessaires pour que les membres du réseau 1 puissent joindre les membres du réseau 2 (et inversement)

  - exemple depuis une machine du réseau 1 (node1.net1.tp3)
  
```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On va créer notre fichier 
$ nano route-enp0s3
```
- **Config de l'interface :**

```bash
10.3.2.0/24 via 10.3.100.1 dev enp0s8
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s3
```

  - exemple depuis une machine du réseau 2 (node1.net2.tp3)
  
```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On va créer notre fichier 
$ nano route-enp0s8
```
- **Config de l'interface :**

```bash
10.3.1.0/24 via 10.3.100.2 dev enp0s3
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s8
```

- Configuration du router.net1.tp3


```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On va créer notre fichier 
$ nano route-enp0s8
```
- **Config de l'interface :**

```bash
10.3.2.0/24 via 10.3.2.254 dev enp0s8
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s8
```

🌞 **Mettre en place les routes par défaut**

- faire en sorte que toutes les machines de votre topologie aient un accès internet, il faut donc :

  - sur les machines du réseau 1, ajouter `router.net1.tp3` comme passerelle par défaut :

    - exemple depuis une machine du réseau 1 (node1.net1.tp3)
  
```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On va créer notre fichier 
$ nano route-enp0s3
```
- **Config de l'interface :**

```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.1.11
NETMASK=255.255.255.0

GATEWAY=10.3.100.1
DNS1=1.1.1.1
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s3
```

- sur les machines du réseau 2, ajouter `router.net2.tp3` comme passerelle par défaut

  - exemple depuis une machine du réseau 2 (node1.net2.tp3)
  
```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On va créer notre fichier 
$ nano route-enp0s8
```
- **Config de l'interface :**

```bash
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.2.11
NETMASK=255.255.255.0

GATEWAY=10.3.100.2
DNS1=1.1.1.1
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s8
```

- sur `router.net2.tp3`, ajouter `router.net1.tp3` comme passerelle par défaut

  - Configuration du router.net2.tp3


```bash
# On rentre dans network-scripts
$ cd /etc/sysconfig/network-scripts

# On va créer notre fichier 
$ nano route-enp0s8
```
- **Config de l'interface :**

```bash
NAME=enp0s9
DEVICE=enp0s9

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.100.2
NETMASK=255.255.255.252

GATEWAY=10.3.100.1
DNS1=1.1.1.1
```
- **Redémarrer l'interface :**

```bash
$ sudo nmcli con reload
$ sudo nmcli con up enp0s8
```

- **Prouver avec un `ping` depuis `node1.net1.tp3` que vous avez bien un accès internet :**

    - $ping google.com

- **Affichage :**

```bash
PING google.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=127 time=34.6 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=127 time=35.5 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=3 ttl=127 time=36.0 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 34.612/35.351/35.988/0.566 ms
```
- **Prouver avec un `traceroute` depuis `node2.net1.tp3` que vous avez bien un accès internet, et que vos paquets transitent bien par `router2.tp3` puis par `router1.tp3` avant de sortir vers internet :**

  - $ip r s

- **Affichage :**

```bash
default via 10.3.100.1 dev enp0s3 proto static metric 100
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.12 metric 100
10.3.2.0/24 via 10.3.100.1 dev enp0s3 proto static metric 100
10.3.100.1 dev enp0s3 proto static scope link metric 100
192.168.194.0/24 dev enp0s8 proto kernel scope link src 192.168.194.51 metric 101
```

# Services Réseau

### Mise en place des services réseau au sein de cette topologie

### Tableau d'adressage

| Machine          | Réseau 1        | Réseau 2        | Réseau 3        |
| ---------------- | --------------- | --------------- | --------------- |
| `node1.net1.tp3` | `10.3.1.11/24`  | nop             | nop             |
| `dhcp.net1.tp3`  | `10.3.1.253/24` | nop             | nop             |
| `router1.tp3`    | `10.3.1.254/24` | nop             | `10.3.100.1/30` |
| `router2.tp3`    | nop             | `10.3.2.254/24` | `10.3.100.2/30` |
| `web.net2.tp3`   | nop             | `10.3.2.101/24` | nop             |
| `dns.net2.tp3`   | nop             | `10.3.2.102/24` | nop             |

- Voici ce que j'ai fait :

```bash
- dhcp dans le réseau 1 ➜ modification et configuration de son interface enp0s3

- web et dns dans le réseau 2 ➜ modification et configuration de leur interface enp0s8
```

# I. DHCP

🌞 **Setup de la machine `dhcp.net1.tp3`**

☀️ **Install et conf du serveur DHCP** sur `dhcp.net1.tp3`

- **Ajouter une route par défaut, qui passe par `router.net1.tp3` :**

  - $cd /etc/sysconfig/network-scripts/

  - $nano ifcfg-enp0s3

- **Config de l'interface :**

```bash
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.1.253
NETMASK=255.255.255.0

GATEWAY=10.3.1.254
DNS1=1.1.1.1
```

  - $nmcli con reload

  - $nmcli con up enp0s3

- **Résultat de la config :**

    - $ping google.com

- **Affichage :**

```bash
[root@DeLima netfoz]# ping google.com
PING google.com (142.250.201.174) 56(84) bytes of data.
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=1 ttl=127 time=33.5 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=2 ttl=127 time=43.9 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 33.457/38.663/43.869/5.206 ms
```
- **Install du serveur :** 

    - $dnf -y install dhcp-server

- **Ajouter, dans la conf, une option DHCP pour donner au client l'adresse de la passerelle du réseau (c'est à dire l'adresse de `router.net1.tp3`) en plus de leur proposer une IP libre :**

    - $vi /etc/dhcp/dhcpd.conf

- **Fichier de config :**

```bash
# create new
# specify domain name
option domain-name     "net1.tp3";

# specify DNS server's hostname or IP address
option domain-name-servers     1.1.1.1;

# default lease time
default-lease-time 600;

# max lease time
max-lease-time 7200;

# this DHCP server to be declared valid
authoritative;

# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    # specify broadcast address
    option broadcast-address 10.3.1.255;
    # specify gateway
    option routers 10.3.1.254;
}
```

$systemctl enable --now dhcpd


- **Autoriser le service DHCP :**

  - $firewall-cmd --add-service=dhcp

  ```bash
  success
  ```

  - $firewall-cmd --runtime-to-permanent

  ```bash
  success
  ```

🌞 **Preuve !**

☀️ **Test du DHCP** sur `node1.net1.tp3`

- **Effectuer une demande d'adresse IP depuis `node1.net1.tp3` :**

- **Modifier l'interface pour qu'elle récupère une IP dynamique, c'est à dire avec DHCP :**

  - $cd /etc/sysconfig/network-scripts/

  - $nano ifcfg-enp0s3

- **Config de l'interface :**

```bash 
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

GATEWAY=10.3.100.1
DNS1=1.1.1.1
```

$nmcli con reload

$nmcli con up enp0s3

- **Montrer l'IP obtenue :**
  
  - $ip a

  ```bash
  2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:82:24:2a brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 516sec preferred_lft 516sec
    inet6 fe80::a00:27ff:fe82:242a/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
  ```

- **Montrer que la table de routage a bien été mise à jour :**

  - $ip r s

  ```bash
  default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.50 metric 102
  10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.50 metric 102
  192.168.194.0/24 dev enp0s8 proto kernel scope link src 192.168.194.50 metric 101
  ```
- **Montrer l'adresse du serveur DNS utilisée :**

  - $vi /etc/dhcp/dhcpd.conf

  ```bash
  [root@Gregersen ~]# cat /etc/resolv.conf
  # Generated by NetworkManager
  search net1.tp3
  nameserver 1.1.1.1
  ```

 - **Prouver que vous avez un accès internet normal avec un `ping` :**

    ```bash
    [root@Gregersen ~]# ping google.com
    PING google.com (142.250.74.238) 56(84) bytes of data.
    64 bytes from par10s40-in-f14.1e100.net (142.250.74.238):   icmp_seq=1 ttl=127 time=32.5 ms
    64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=127 time=32.0 ms
    ^C
    --- google.com ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 1003ms
    rtt min/avg/max/mdev = 31.990/32.222/32.455/0.232 ms
    ```

# II. Serveur Web

## 1. Installation

**Install et config sur la machine `web.net2.tp3`.**

🌞 **Installation du serveur web NGINX**

- Installer le paquet `nginx` :
  
  - $dnf update 
  - $dnf install nginx

## 2. Page HTML et racine web

🌞 **Création d'une bête page HTML**

- **Créer le dossier `/var/www/efrei_site_nul/`**

  - $cd var
  - $mkdir -p www/efrei_site_nul

- **Faire appartenir le dossier à l'utilisateur `nginx` :**

  - $chown -R nginx:nginx www/efrei_site_nul

- **Créer un fichier texte `/var/www/efrei_site_nul/index.html` avec une phrase au choix à l'intérieur :**

  - $echo "<p>coucou EFREI</p>" > index.html
  
- **Faire appartenir ce fichier à l'utilisateur `nginx` :**

  - $chown nginx:nginx index.html

## 3. Config de NGINX

🌞 **Création d'un fichier de configuration NGINX**

- **Créer le fichier `/etc/nginx/conf.d/web.net2.tp3.conf` :**

  - $nano /etc/nginx/conf.d/web.net2.tp3.conf

- **Contenu du fichier :**

```nginx
  server {
      # on indique le nom d'hôte du serveur
      server_name   web.net2.tp3;

      # on précise sur quelle IP et quel port on veut que le site soit dispo
      listen        10.3.2.101:80;

      location      / {
          # on indique l'endroit où se trouve notre racine web
          root      /var/www/efrei_site_nul;

          # et on indique le nom de la page d'accueil, pour pas que le client ait besoin de le préciser explicitement
          index index.html;
      }
  }
```

## 4. Firewall

🌞 **Ouvrir le port nécessaire dans le firewall**

- **Ouvrir à l'aide d'une commande le port 80 dans le firewall de `web.net2.tp3` :**

```bash
[root@Cassubie ~]# firewall-cmd --add-port=80/tcp --permanent
success
```
- **Vérifier avec une deuxième commande que le port est bien actuellement ouvert dans le firewall :**

```bash
[root@Cassubie ~]# firewall-cmd --reload
success
```

## 5. Test

🌞 **Démarrez le service NGINX !**

  - $systemctl start nginx

🌞 **Test local**

```bash
[root@Cassubie ~]# curl http://10.3.2.101
<p>coucou EFREI</p>
```

🌞 **Accéder au site web depuis un client**

- **Accéder au site web depuis une machine du réseau 1 :**

  - Exemple : node1.net1.tp3

```bash
[root@Gregersen netfoz]# curl http://10.3.2.101
<p>coucou EFREI</p>
```

🌞 **Avec un nom ?**

- **Utiliser le fichier `hosts` de votre machine client pour accéder au site web en saissant `http://web.net2.tp3` :**

  - $nano /etc/hosts
  
- **Contenu du fichier :**

```hash
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.3.2.101  web.net2.tp3
```

- **Saisie de l'URL et affichage :**
```bash
[root@Gregersen netfoz]# curl http://web.net2.tp3
<p>coucou EFREI</p>
```

# III. Serveur DNS

## 1. Install

**L'installation et la configuration qui va suivre sera réalisé seulement sur `dns.net2.tp3`.**

**Installation du serveur DNS :***

```bash
# On s'assure que la machine est bien à jour
$ sudo dnf update -y

# On installe le serveur DNS
$ sudo dnf install -y bind bind-utils
```

## 2. Config

- **Configuration du fichier principal :**

  - $nano /etc/named.conf

- **Affichage :**

```bash
options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };
        recursion yes; 

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "net2.tp3" IN {
     type master;
     file "net2.tp3.db";
     allow-update { none; };
     allow-query {any; };
};

zone "2.3.10.in-addr.arpa" IN {
     type master;
     file "net2.tp3.rev";
     allow-update { none; };
     allow-query { any; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

- **Configuration du fichier de zone :**

  - $nano /var/named/net2.tp3.db

- **Affichage :**
  
```bash
$TTL 86400
@ IN SOA dns.net2.tp3. admin.net2.tp3. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.net2.tp3.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns        IN A 10.3.2.102
web        IN A 10.3.2.101
```

- **Configuration du fichier de zone inverse :**

  - $nano /var/named/net2.tp3.rev

- **Affichage :**

```bash
$TTL 86400
@ IN SOA dns.net2.tp3. admin.net2.tp3. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.net2.tp3.

;Reverse lookup for Name Server
102   IN PTR dns.net2.tp3.
101   IN PTR web.net2.tp3.
```

- **Une fois que ces 3 fichiers ont été mis en place, on va démarrer tout de suite le service DNS :**

  - $systemctl start named

## 3. Firewall

🌞 **Ouvrir le port nécessaire dans le firewall**

- **Ouvrir à l'aide d'une commande le port UDP dans le firewall de dns.net2.tp3:**

```bash
[root@Johnsson ~]# firewall-cmd --add-port=53/udp --permanent
success
```
- **Vérifier avec une deuxième commande que le port est bien actuellement ouvert dans le firewall :**

```bash
[root@Johnsson ~]# firewall-cmd --reload
success
```

## 4. Test

🌞 **Depuis l'une des machines clientes du réseau 1** (par exemple `node1.net1.tp3`)

- **Assurez-vous de purger votre fichier `hosts` de vos éventuelles précédentes modifications.**

  - **Contenu du fichier hosts avant d'utiliser `dig` et `curl` :**

```bash
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6                                
```

- **Utiliser `dig` pour trouver à quelle IP correspond le nom `web.net2.tp3` et utiliser `curl` pour visiter le site web sur `web.net2.tp3` en utilisant son nom :**

```nginx
[netfoz@Gregersen ~]# dig web.net2.tp3 @10.3.2.102

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3 @10.3.2.102
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62296
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 6806204aa66ed304010000006554947922a8260fc5b4bc0b (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 160 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Wed Nov 15 10:50:49 CET 2023
;; MSG SIZE  rcvd: 85

[root@Gregersen network-scripts][root@Gregersen netfoz]# curl http://web.net2.tp3@10.3.2.101
<p>coucou EFREI</p>
```

## 5. DHCP my old friend

🌞 **Editez la configuration du serveur DHCP sur `dhcp.net1.tp3`**

- **Changer l'adresse du serveur DNS qui est donnée au client :**

```nginx
option domain-name     "net2.tp3";

# specify DNS server's hostname or IP address
option domain-name-servers     10.3.2.102;

# default lease time
default-lease-time 600;

# max lease time
max-lease-time 7200;

# this DHCP server to be declared valid
authoritative;

# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    # specify broadcast address
    option broadcast-address 10.3.1.255;
    # specify gateway
    option routers 10.3.1.254;
}
```

- $systemctl enable --now dhcpd
- $nmcli con reload 
- $nmcli con up enp0s3

- **Prouver que ça fonctionne avec un `dig` depuis un client qui a fraîchement récupéré une IP :**

```nginx
[root@Gregersen netfoz]# dig web.net2.tp3

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 34568
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 22e1d06e7f138d26010000006554e16e97a1a02deb8c9e47 (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 37 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Wed Nov 15 16:19:10 CET 2023
;; MSG SIZE  rcvd: 85
```






